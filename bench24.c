// Heavily (abusively) adapted from http://www.ezcodesample.com/abs/ANSCoder.txt
// This is a hack job for a PoC, if you want to play with ANS start with the source ^^ 
// Original header below

// ANSCoder.cpp Asymmetric Numeral System Coder
// (C) 2009, Andrew Polar under GPL ver. 3.
// Released Feb. 05, 2009.
//
//   LICENSE
//
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of the GNU General Public License as
//   published by the Free Software Foundation; either version 3 of
//   the License, or (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   General Public License for more details at
//   Visit <http://www.gnu.org/copyleft/gpl.html>.
//
// This is implementation of the concept of ANS coder suggested by Jarek Duda at 
// http://demonstrations.wolfram.com/DataCompressionUsingAsymmetricNumeralSystems/
// and reduced to practice by Andrew Polar. 
// First  version May 24, 2008
// This version   Feb  5, 2009
//

#include <inttypes.h>
#include <math.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


typedef   uint8_t u8;
typedef uint16_t u16;
typedef uint64_t u64;

#define  U8  PRIu8
#define U16 PRIu16
#define U64 PRIu64

size_t fread_fully_u8(u8 *dest, FILE *src){
	size_t ret, peek;
	fseek(src, 0, SEEK_END);
	peek=ftell(src);
	rewind(src);
	if(dest){
		ret=fread(dest, 1, peek, src);
		fclose(src);
		return ret;
	}
	else
		return peek;
}

//tl;dr precomputed static tables are used (global variables below) instead of being based on input
//rawfreq is stored instead of freq to allow for the possibility of implementing non-ANS encoding for comparison, aka huffman TODO

//ANS working memory to be generated from the other global variables
int *freq_static=NULL;
int *chart_static=NULL;
int *sizes_static=NULL;
int **descend_static=NULL;
u8  *ascendSymbol_static=NULL;
int *ascendState_static=NULL;

//static tables generated from enwik8 using gen_static_tables
int nSymbols_static=256;
int PROBABILITY_PRECISION_static=12;
int STATE_PRECISION_static=13;
int MAX_STATE_static=8191;
int rawfreq[]={0, 0, 0, 0, 0, 0, 0, 0, 0, 4358, 1128023, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 13519824, 14930, 24744, 46190, 3993, 16899, 479245, 946508, 221072, 221390, 235695, 8932, 787826, 326098, 794548, 427309, 376132, 425137, 266697, 156663, 145926, 157908, 134227, 121640, 146017, 247003, 333366, 572842, 267136, 400912, 267136, 11551, 300, 339329, 208784, 340203, 169944, 177160, 148917, 145799, 152512, 251957, 76029, 57854, 122102, 184894, 119779, 89984, 183054, 10724, 141730, 295942, 308336, 78012, 46186, 102017, 10910, 21602, 25341, 1945220, 17862, 1945253, 3664, 49420, 379, 5712026, 1004067, 2284625, 2400013, 8001205, 1360778, 1465591, 2836146, 5227649, 83366, 401138, 3047318, 1944706, 4909103, 5176143, 1541263, 260243, 4506880, 4347421, 6154908, 2017868, 693456, 930909, 223305, 1067379, 99229, 90906, 435450, 90958, 2463, 0, 19280, 8912, 12048, 17970, 5051, 3612, 3365, 3233, 4479, 3407, 2792, 2646, 3863, 4157, 1327, 2791, 5828, 3685, 2219, 5742, 7535, 5939, 2724, 3156, 3976, 9003, 2503, 2959, 5059, 3426, 2831, 2652, 4155, 9916, 2690, 3598, 8370, 3725, 4518, 6296, 6091, 12997, 4803, 3456, 2533, 6996, 3399, 4074, 14009, 6232, 6200, 8685, 5379, 7836, 4459, 3594, 18440, 5917, 6512, 6620, 8825, 7835, 7452, 4386, 0, 0, 5424, 48988, 5561, 5848, 151, 304, 15, 2913, 1688, 1469, 661, 93, 12548, 5558, 56224, 21420, 221, 128, 80, 421, 1214, 27583, 8573, 6275, 323, 656, 16, 0, 18, 0, 18990, 6448, 12321, 16915, 1788, 6506, 4254, 2990, 2415, 1894, 620, 2227, 3178, 1117, 0, 155, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

//this parameter is a trade off between compression ratio and speed.
//the higher the value the better compression.
//for static tables this needs to be 4 so that state fits in 13 bits
const unsigned char PRECISION = 4; //must not be less than 2

//functions used verbatim from the cpp source (once converted to c)
double round(double x){
	if((x - floor(x)) >= 0.5)
		return ceil(x);
	else
		return floor(x);
}
int bitlen(unsigned D){
	int len = 0;
	while (D > 0){
		D >>= 1;
		++len;
	}
	return len;
}
void GetOrder(int* data, int* order, int size){
	int min = data[0];
	int max = data[0];
	for(int i=0; i<size; ++i){
		if(min > data[i])
			min = data[i];
		if(max < data[i])
			max = data[i];
	}
	if(min == max){
		for(int i=0; i<size; ++i)
			order[i] = i;
		return;
	}
	++max;

	int* tmp = (int*)malloc(size*sizeof(int));
	memcpy(tmp, data, size*sizeof(int));
	
	for(int k=0; k<size; ++k){
		int pos = 0;
		min = tmp[pos];
		for(int i=0; i<size; ++i){
			if(min > tmp[i]){
				min = tmp[i];
				pos = i;
			}
		}
		order[k] = pos;
		tmp[pos] = max;
	}
	if(tmp)
		free(tmp);
}
void MakeChart(int* chart, int* freq, int* sizes, int nSymbols, int PROBABILITY_PRECISION, int MAX_STATE){
	for(int i=0; i<nSymbols; ++i){
		sizes[i] = 1;  //this is because table starts from 1 not from 0
	}

	int* order = (int*)malloc(nSymbols*sizeof(int));
	GetOrder(freq, order, nSymbols);

	int denominator = 1<<PROBABILITY_PRECISION;
	for(int k=0; k<=MAX_STATE; ++k)
		chart[k] = denominator;

    int state;
	for(int j=1; j<=MAX_STATE; ++j){
		for(int i=0; i<nSymbols; ++i){
			state = (j << PROBABILITY_PRECISION)/freq[order[i]]; 
			if(state < 2)
				state = 2;
			if(state <= MAX_STATE){
				if(chart[state] == denominator){
					chart[state] = order[i];
					++sizes[order[i]];
				}
				else{
					do {
						++state;
						if(state > MAX_STATE)
							break;
						if(chart[state] == denominator){
							chart[state] = order[i];
							++sizes[order[i]];
							break;
						}
					}while (1);
				}
			}
		}
	}

	if(order)
		free(order);
}
void MakeDESCENDTable(int** descend, int* chart, int* sizes, int nSymbols, int PROBABILITY_PRECISION,
					  int MAX_STATE){

	int denominator = 1<<PROBABILITY_PRECISION;
	int* order = (int*)malloc(nSymbols*sizeof(int));
	for(int k=0; k<nSymbols; ++k)
		order[k] = 1;

	for(int j=2; j<=MAX_STATE; ++j){
		if(chart[j] != denominator){
			descend[chart[j]][order[chart[j]]++] = j;
		}
	}

	if(order)
		free(order);
}
void MakeASCENDTable(int** descend, u8* ascendSymbol,
					 int* ascendState, int nSymbols, int* sizes){

	ascendSymbol[0] = 0;
	ascendState [0] = 0;
	for(int i=0; i<nSymbols; ++i){
		int counter = 1;
		int j = 1;
		for(int k=0; k<sizes[i]-1; ++k){
			ascendSymbol[descend[i][j]] = i;
			ascendState [descend[i][j]] = counter++;
			++j;
		}
	}
}
//end of verbatim

int EncodeDataStatic(u8 *source, int data_size, unsigned char* result, int *result_size){
	int res_size=0;

	//encoding
	int encoding_correct = 1;
	int state = MAX_STATE_static;
	unsigned char byte = 0;
	unsigned char bit  = 0;
	int control_MASK = 1 << (STATE_PRECISION_static-1);

	for(int i=0; i<data_size; ++i){
		while (state > sizes_static[source[i]]-1){
			++bit;
			byte |= state & 1;
			if(bit == 8){
				result[res_size++] = byte;
				bit = 0;
				byte = 0;
			}
			else{
				byte <<=1;
			}
			state >>= 1;
		}
		state = descend_static[source[i]][state];
		if(state < control_MASK){
			fprintf(stderr, "problem with data symbol %d %d %d\n", i, source[i], state);
			encoding_correct = 0;
			break;
		}
	}
	int byte_offset=0; 
	if(bit){
		byte_offset = 8 - bit;
		result[res_size++] = byte << (7 - bit);
	}
	u16 state_serial=state|(byte_offset<<13);
	memcpy(result + res_size, &state_serial, 2);
	res_size += 2;
	//end encoding
	*result_size=res_size;
	return encoding_correct;
}
//pass decoded length in decoded_size, simulate not including size
void DecodeDataStatic(u8 *data, int data_size, u8 *decoded, int *decoded_size){
	int dec_size=0;

	int STATE_PRECISION = STATE_PRECISION_static;

	dec_size=*decoded_size;

	//decoding
	int state;
	u16 state_serial;
	memcpy(&state_serial, data + data_size - 2, 2);
	state=state_serial&0x1FFF;
	int byte_offset=state_serial>>13;

	int MASK = 1<<(STATE_PRECISION-1);
	unsigned char shift = byte_offset;
	int counter = data_size - 3;
	for(int i=dec_size-1; i>=0; --i){
		decoded[i] = ascendSymbol_static[state];
		state  = ascendState_static[state];
		while (state < MASK){
			state <<= 1;
			state |= (data[counter] & (1<<shift))>>shift;
			++shift;
			if(shift == 8){
				shift = 0;
				--counter;
			}
		}
	}
	//end decoding
	*decoded_size=dec_size;
}

//gen_rawfreq and gen_freq are repurposed from the original function CollectStatistics
//gen_rawfreq is used by gen_static_tables to generate a raw frequency histogram
void gen_rawfreq(u8 *data, int data_size, int *freq){
	memset(freq, 0x00, 256*sizeof(int));
	for(int i=0; i<data_size; ++i)
		++freq[data[i]];
}
//gen_freq is used by init_static to generate freq used by ANS
void gen_freq(int *rawfreq, int* freq, int PROBABILITY_PRECISION){
	int data_size=0;
	for(int i=0; i<256; ++i){
		freq[i]=rawfreq[i];
		data_size+=rawfreq[i];
	}
	double coef = (double)(1<<PROBABILITY_PRECISION);
	coef /= (double)(data_size);
	for(int j=0; j<256; ++j){
		freq[j] = (int)(double)((freq[j])*coef);
	}
	int total = 0;
	for(int k=0; k<256; ++k){
		total += freq[k];
	}
	int diff = (1<<PROBABILITY_PRECISION) - total;
	
	int maxPos = 0;
	int max = freq[maxPos];
	for(int k=0; k<256; ++k){
		if(max < freq[k]){
			max = freq[k];
			maxPos = k;
		}
	}
	freq[maxPos] += diff;

	for(int k=0; k<256; ++k){
		if(freq[k] == 0){
			freq[k] = 1;
			freq[maxPos]--;
		}
	}
}

//use input to generate new static tables to stdout, to be pasted directly into source
int gen_static_tables(FILE *fin){
	u8 *data;
	int len;
	len=fread_fully_u8(NULL, fin);
	data=malloc(len);
	len=fread_fully_u8(data, fin);
	
	int nSymbols = 256;
	int PROBABILITY_PRECISION = bitlen(nSymbols-1) + PRECISION;//fix for off-by-1 bitlen()
	int STATE_PRECISION = PROBABILITY_PRECISION + 1;
	int MAX_STATE = (1<<STATE_PRECISION)-1;

	int freq[256];
	printf("int nSymbols_static=%d;\n", nSymbols);
	printf("int PROBABILITY_PRECISION_static=%d;\n", PROBABILITY_PRECISION);
	printf("int STATE_PRECISION_static=%d;\n", STATE_PRECISION);
	printf("int MAX_STATE_static=%d;\n", MAX_STATE);
	//CollectStatistics(data, len, freq, nSymbols, PROBABILITY_PRECISION);
	gen_rawfreq(data, len, freq);

	printf("int rawfreq[]={%d", freq[0]);
	for(int i=1;i<nSymbols; ++i){
		printf(", %d", freq[i]);
	}
	printf("};\n");
	free(data);
	return 1;
}

//initialise static tables from rawfreq
void init_static(){
	//Make freq
	freq_static=malloc(256*sizeof(int));
	gen_freq(rawfreq, freq_static, PROBABILITY_PRECISION_static);
	
	//Make CHART
	chart_static = (int*)malloc((MAX_STATE_static+1) * sizeof(int));
	sizes_static = (int*)malloc(nSymbols_static*sizeof(int));
	MakeChart(chart_static, freq_static, sizes_static, nSymbols_static, PROBABILITY_PRECISION_static, MAX_STATE_static);

	//Make DESCEND table
	descend_static = (int**)malloc(nSymbols_static * sizeof(int*));
	for(int i=0; i<nSymbols_static; ++i){
		descend_static[i] = (int*)malloc((sizes_static[i]) * sizeof(int));
		memset(descend_static[i], 0x00, (sizes_static[i]) * sizeof(int));
	}
	MakeDESCENDTable(descend_static, chart_static, sizes_static, nSymbols_static, PROBABILITY_PRECISION_static, MAX_STATE_static);
	
	//Make of ASCEND table
	ascendSymbol_static = (u8*)malloc((MAX_STATE_static+1)*sizeof(int));
	ascendState_static  = (int*)malloc((MAX_STATE_static+1)*sizeof(int));
	MakeASCENDTable(descend_static, ascendSymbol_static, ascendState_static, nSymbols_static, sizes_static);
}
//end of static table functions

//24 byte target functions follow

struct ans_wip{
	u8 enc[50];
	u8 dec[200];
	u8 byte, bit;
	int state, enc_size, dec_size, encoding_correct;
};
//reset ans struct to start a new encode
void ans_reset(struct ans_wip *ans){
	ans->encoding_correct=1;
	ans->state=MAX_STATE_static;
	ans->byte=0;
	ans->bit=0;
	ans->enc_size=0;
	ans->dec_size=0;
}
//try encoding the next UTF-8 character, return 1 if the string still fits into 24 bytes, 0 otherwise
//length takes up a byte and state+offset take up 2 bytes, so the encoding we're measuring can take no more than 21 bytes
int ans_encode24(struct ans_wip *ans, u8* source, int data_size){
	struct ans_wip cpy;
	memcpy(&cpy, ans, sizeof(struct ans_wip));
	//encoding
	int control_MASK = 1 << (STATE_PRECISION_static-1);

	for(int i=0; i<data_size; ++i){
		while (cpy.state > sizes_static[source[i]]-1){
			++cpy.bit;
			cpy.byte |= cpy.state & 1;
			if(cpy.bit == 8){
				cpy.enc[cpy.enc_size++] = cpy.byte;
				cpy.bit = 0;
				cpy.byte = 0;
			}
			else{
				cpy.byte <<=1;
			}
			cpy.state >>= 1;
		}
		cpy.state = descend_static[source[i]][cpy.state];
		if(cpy.state < control_MASK){
			fprintf(stderr, "problem with data symbol %d %d %d\n", i, source[i], cpy.state);
			cpy.encoding_correct = 0;
			break;
		}
	}
	cpy.dec_size+=data_size;
	if(cpy.enc_size<=21){
		//it fits, use it
		memcpy(ans, &cpy, sizeof(struct ans_wip));
		return 1;
	}
	else
		return 0;//does not fit, throw away this encode
}



//pack input into as few 24 byte chunks as possible using ans
//Multi-byte UTF-8 characters cannot span chunks
void ans24(u8 *in, u64 inlen){
	struct ans_wip ans;
	u64 c_inline=0, c_ans=0, i=0;
	int clen;
	ans_reset(&ans);
	while(i<inlen){//for every byte of input
		//find next UTF-8 character
		if((in[i]>>7)==0)
			clen=1;
		else if((in[i]>>5)==6)
			clen=2;
		else if((in[i]>>4)==14)
			clen=3;
		else if((in[i]>>3)==30)
			clen=4;
		else{
			fprintf(stderr, "Invalid UTF-8 character, exiting\n");
			break;
		}
		if(i+clen>inlen)//skip last character if it's incomplete
			break;
		if(ans_encode24(&ans, in+i, clen)){
			i+=clen;
		}
		else{//character overflows chunk, create a new chunk and add it to it
			if(ans.dec_size<24){//encoding is larger than what's being encoded
				//fill inline as much as possible before generating it
				while(i<inlen){
					//find next UTF-8 character
					if((in[i]>>7)==0)
						clen=1;
					else if((in[i]>>5)==6)
						clen=2;
					else if((in[i]>>4)==14)
						clen=3;
					else if((in[i]>>3)==30)
						clen=4;
					if(i+clen>inlen)//skip last character if it's incomplete
						break;
					ans.dec_size+=clen;
					if(ans.dec_size>23){
						ans.dec_size-=clen;
						++c_inline;
						break;
					}
					i+=clen;
				}
				if(i>=inlen)
					break;
			}
			else
				++c_ans;
			ans_reset(&ans);
			ans_encode24(&ans, in+i, clen);
			i+=clen;
		}
	}
	if(ans.dec_size){//complete last string
		if(ans.dec_size<24)
				++c_inline;
			else
				++c_ans;
	}
	printf("\nans24 results:\n");
	printf("inline count: %"U64"\n", c_inline);
	printf("packed count: %"U64"\n", c_ans);
	printf("total  count: %"U64"\n", (c_inline+c_ans));
	printf("Average bytes stored per chunk: %f\n", ((inlen*1.0)/(c_inline+c_ans)));
}

//simulate compact_str, input into as few compact_str chunks as possible
void compact_str24(u8 *in, u64 inlen){
	u64 c_inline=0, c_packed=0, i=0;
	int clen;

	int compact_str_len=0;
	int firstascii=0;

	while(i<inlen){//for every byte of input
		//find next UTF-8 character
		if((in[i]>>7)==0)
			clen=1;
		else if((in[i]>>5)==6)
			clen=2;
		else if((in[i]>>4)==14)
			clen=3;
		else if((in[i]>>3)==30)
			clen=4;
		else{
			fprintf(stderr, "Invalid UTF-8 character, exiting\n");
			break;
		}
		if(i+clen>inlen)//skip last character if it's incomplete
			break;
		if(compact_str_len==0){
			firstascii=(clen==1?1:0);
		}
		compact_str_len+=clen;
		
		if(compact_str_len==24 && firstascii){//packed
			++c_packed;
			compact_str_len=0;
		}
		else if(compact_str_len>23){
			compact_str_len-=clen;
			++c_inline;
			firstascii=(clen==1?1:0);
			compact_str_len=clen;
		}
		i+=clen;
	}
	if(compact_str_len)//complete last string
		++c_inline;

	printf("\ncompact_str24 results:\n");
	printf("inline count: %"U64"\n", c_inline);
	printf("packed count: %"U64"\n", c_packed);
	printf("total  count: %"U64"\n", (c_inline+c_packed));
	printf("Average bytes stored per chunk: %f\n", ((inlen*1.0)/(c_inline+c_packed)));
}

//multiple 
void upper_bound24(u8 *in, u64 inlen, char *function, int ub){
	u64 c_inline=0, i=0;
	int clen;

	int compact_str_len=0;
	while(i<inlen){//for every byte of input
		//find next UTF-8 character
		if((in[i]>>7)==0)
			clen=1;
		else if((in[i]>>5)==6)
			clen=2;
		else if((in[i]>>4)==14)
			clen=3;
		else if((in[i]>>3)==30)
			clen=4;
		else{
			fprintf(stderr, "Invalid UTF-8 character, exiting\n");
			break;
		}
		if(i+clen>inlen)//skip last character if it's incomplete
			break;
		compact_str_len+=clen;
		if(compact_str_len>ub){
			++c_inline;
			compact_str_len=clen;
		}
		i+=clen;
	}
	if(compact_str_len)//complete last string
		++c_inline;

	printf("\n%s results:\n", function);
	printf("inline count: %"U64"\n", c_inline);
	printf("Average bytes stored per chunk: %f\n", ((inlen*1.0)/(c_inline)));
}

int main(int argc, char *argv[]){
	FILE *fin;
	u8 *in;
	u64 inlen;

	if(argc==3 && (strcmp(argv[1], "--gen-static")==0)){
		fin=fopen(argv[2], "rb");
		gen_static_tables(fin);
		return 0;
	}
	if(argc!=2 || (strcmp(argv[1], "-h")==0) || (strcmp(argv[1], "--help")==0)){
		fprintf(stderr, "bench24: Compare implementations of inlined strings\n");
		fprintf(stderr, "         by space-efficiency\n\n");
		fprintf(stderr, "Usage:\n");
		fprintf(stderr, "Benchmark using a file: bench24 file\n");
		fprintf(stderr, "  Greedily put file into as few inline strings as possible,\n");
		fprintf(stderr, "  the fewer inline strings are used the more space-efficient\n");
		fprintf(stderr, "  the representation is. UTF-8 aware\n\n");
		fprintf(stderr, "Generate new static table from file: bench24 --gen-static file\n");
		fprintf(stderr, "  Generate a new set of static compression tables based on the\n");
		fprintf(stderr, "  characteristics of a file. These are used instead of adapting\n");
		fprintf(stderr, "  to the input as the overhead kills the viability of adaptive\n");
		fprintf(stderr, "  (output to be pasted into source and recompiled to use):\n\n");
		return 1;
	}
	init_static();
	fin=fopen(argv[1], "rb");
	inlen=fread_fully_u8(NULL, fin);
	in=malloc(inlen);
	fread_fully_u8(in, fin);
	upper_bound24(in, inlen, "kstring_performance24", 15);
	upper_bound24(in, inlen, "kstring_max24", 22);
	upper_bound24(in, inlen, "smartstring24", 23);
	compact_str24(in, inlen);
	ans24(in, inlen);
	return 0;
}
