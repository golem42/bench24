bench24 
=
This program benchmarks various inlined string formats (formats that try to store small strings on the stack) that consume 24 bytes. Rust has many implementations of inlined strings of this form and this program simulates some of them for comparative purposes. The forms compared are kstring (performance and max, inlining up to 15 byte and 22 byte strings respectively), smartstring, compact_str, and ans24 which is a fictional type that uses ANS entropy encoding to maximise space-efficiency at a large time cost.

Pass the program a file and it will count how many inlined strings of each format it would take to store the entire file. This is not the typical way inlined strings are used (large strings are stored on the heap), but it's a handy way to be able to benchmark the space-efficiency of a format. UTF-8 is obeyed, a multi-byte character will not be split across two strings.

ANS24
===
This format doesn't actually exist, it's simulated here as an example of the sort of space-efficiency that can be achieved if a compressed inline string was created. tl;dr ANS encoding is roughly arithmetic coding but faster, AFAIK it's state of the art when it comes to relatively quick entropy-coding: https://en.wikipedia.org/wiki/Asymmetric_numeral_systems

The form of ANS used for ans24 is tANS, fixed static tables are used so that they don't need to be encoded in each string. The static tables are generated from enwik8, a chunk of wikipedia (mostly natural language also with XML-tags, wiki markdown, HTML encoding). enwik8 was chosen to reasonably represent the latin alphabet and structured data, other types of string will perform poorly (decaying to smartstring space-efficiency worst-case). Using a static table minimises overhead, the only things that need to be transmitted other than the raw encoding is the decoded length and the states of the state and bitshift variables at end of the encode.
* u8: Length in bytes, length<=23 stored inline as-is, length=0xFF heap allocated, otherwise stored packed using ANS
* u8[21]: The raw encoding has 21 bytes it can be stored in
* u16: End state (13 bits) and bitshift (3 bits) packed into two bytes

Quick results
==
Showing enwik8 as input is a cheeky example as the static table used by ans24 was tuned specifically for enwik8 so it's obviously going to perform well. Feeding the sourcecode to the program is a more reasonable representation of what to expect from the average case.

bench24.c as input:
===
kstring_performance24 results:
inline count: 1224
Average bytes stored per chunk: 14.995915

kstring_max24 results:
inline count: 835
Average bytes stored per chunk: 21.982036

smartstring24 results:
inline count: 799
Average bytes stored per chunk: 22.972466

compact_str24 results:
inline count: 1
packed count: 764
total  count: 765
Average bytes stored per chunk: 23.993464

ans24 results:
inline count: 172
packed count: 494
total  count: 666
Average bytes stored per chunk: 27.560060

enwik8 as input
===
kstring_performance24 results:
inline count: 6668521
Average bytes stored per chunk: 14.995829

kstring_max24 results:
inline count: 4546433
Average bytes stored per chunk: 21.995265

smartstring24 results:
inline count: 4348729
Average bytes stored per chunk: 22.995225

compact_str24 results:
inline count: 40997
packed count: 4127697
total  count: 4168694
Average bytes stored per chunk: 23.988328

ans24 results:
inline count: 76699
packed count: 2906921
total  count: 2983620
Average bytes stored per chunk: 33.516333
